package io.vincentpalazzo.template.util;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class Constant {

    public static final String RESOURCE_BUNDLE = "RESOURCE_BUNDLE";
    public static final String NAME_FILE_PROPERTIS = "configuration.properties";

    //RESOURCES VALUE
    public static final String MENU_FILE_VALUE = "MENU_FILE";
    public static final String MENU_INFO_VALUE = "MENU_INFO";
    public static final String MENU_I_EXIT_VALUE = "MANU_I_EXIT";
    public static final String MENU_I_DEV_VALUE = "MANU_I_DEV";
    public static final String NAME_DEV_VALUE = "NAME_DEV_VALUE";
    public static final String ICON_DEV_PATH = "ICON_DEV_PATH";
    public static final String SITE_DEV_INFO = "SITE_DEV_INFO";
    public static final String GITHUB_DEV_INFO = "GITHUB_DEV_INFO";
    public static final String TITLE_TASK_PANE_DESCRIPTION = "TITLE_TASK_PANE_DESCRIPTION";


    //Constant for action
    public static final String VIEW_DEV_ACTION_KEY = "VIEW_DEV_ACTION_KEY";
    public static final String EXIT_ACTION_KEY = "EXIT_ACTION_KEY";

    public static final String TITLE_DIALOG_DEV_INFO = "TITLE_DIALOG_DEV_INFO";
    public static final String ACTION_OPEN_LINK = "ACTION_OPEN_LINK";
}
