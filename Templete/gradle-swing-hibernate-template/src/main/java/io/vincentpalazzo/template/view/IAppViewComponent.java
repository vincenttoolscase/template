package io.vincentpalazzo.template.view;

import io.vincentpalazzo.template.view.eception.ViewException;

/**
 * @author https://github.com/vincenzopalazzo
 */
public interface IAppViewComponent {

    void initView() throws ViewException;
    void initComponent() throws ViewException;

    void initActions() throws ViewException;
}
