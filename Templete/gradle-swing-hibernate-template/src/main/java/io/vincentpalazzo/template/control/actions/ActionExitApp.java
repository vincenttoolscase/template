package io.vincentpalazzo.template.control.actions;

import io.vincentpalazzo.template.App;
import io.vincentpalazzo.template.util.AppResourceManager;
import io.vincentpalazzo.template.util.Constant;
import io.vincentpalazzo.template.util.IAppResourceManager;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class ActionExitApp extends AbstractAction {

    private IAppResourceManager resourceManager = (IAppResourceManager) App.getInstance().getInstanceObject(AppResourceManager.class);

    public ActionExitApp(){
        putValue(Action.NAME, resourceManager.getResourceString(Constant.MENU_I_EXIT_VALUE));
        //TODO another value
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
