package io.vincentpalazzo.template.util.aop.interseptors;

import io.vincentpalazzo.template.pesistence.hibernate.DAOUtilHibernate;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class HibernateInterceptor implements MethodInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(HibernateInterceptor.class);

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        LOGGER.debug("Was called the method: " + invocation.getMethod().getName());

        try {
            DAOUtilHibernate.beginTransaction();
            LOGGER.debug("The transaction with hibernate is open");
            return invocation.proceed();
        }finally {
            DAOUtilHibernate.commit();
            LOGGER.debug("The transaction with hibernate was closed");

        }
    }

}
