package io.vincentpalazzo.template.view;

import mdlaf.MaterialLookAndFeel;
import mdlaf.utils.MaterialColors;

import javax.swing.*;
import java.awt.*;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class AppTheme extends JFrame {

    private Color primaryDarkBackground = MaterialColors.COSMO_BLACK;
    private Color primaryDarkForeground = MaterialColors.COSMO_LIGTH_GRAY;


    public AppTheme() throws HeadlessException {
        configureTheme();
    }

    public void configureTheme(){
        try {
            JDialog.setDefaultLookAndFeelDecorated(true);
            UIManager.setLookAndFeel(new MaterialLookAndFeel());
            initStyleLigthApp();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    protected   void initStyleLigthApp(){

    }

    protected void initStyleDarkApp(){

        UIManager.put("Panel.background", primaryDarkBackground);
        UIManager.put("Panel.border", primaryDarkForeground);

        UIManager.put("TaskPane.contentBackground", primaryDarkBackground);
        //library is confusing, hence this comment to explain: arrow > "right" is collapsed = "no_collapsed"; arrow "down" is expand = "yes_collapsed"
        UIManager.put("TaskPane.background", primaryDarkBackground);
        UIManager.put("TaskPane.foreground", primaryDarkForeground);

        UIManager.put("Label.background", primaryDarkBackground);
        UIManager.put("Label.foreground", primaryDarkForeground);

    }
}
