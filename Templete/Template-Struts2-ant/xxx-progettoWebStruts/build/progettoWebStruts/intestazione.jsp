<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.0//EN"
               "DTD-xhtmlbasic/xhtml-basic10.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>
    <html:base />
    <meta name="author" content="Autore - autore@indirizzo.it" />
    <title>${param.titolo}</title>
    <link rel="stylesheet" href="stile.css" type="text/css" />
</head>

<body>
