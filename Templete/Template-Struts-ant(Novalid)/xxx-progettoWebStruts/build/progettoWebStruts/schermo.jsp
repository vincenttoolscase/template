<?xml version="1.0" encoding="iso-8859-1" ?>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!-- ************** intestazione ************** -->
<c:import url="intestazione.jsp">
    <c:param name="titolo" value="Titolo" />
</c:import>
<!-- ******************************************* -->

<h2>Pagina</h2>


<!-- ************** pie di pagina ************** -->
<c:import url="pieDiPagina.jsp" />
<!-- ******************************************* -->
