package it.unibas.progetto.persistenza;

import it.unibas.progetto.eccezioni.DAOException;
import it.unibas.progetto.modello.Utente;

public interface IDAOUtente extends IDAOGenerico<Utente> {
     
    public Utente findByNomeUtente(String nomeUtente) throws DAOException;

    public void save(Utente utente) throws DAOException;

    public void delete(Utente utente) throws DAOException;

}
