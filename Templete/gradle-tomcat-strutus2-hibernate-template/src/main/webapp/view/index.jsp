<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <![endif]-->
    <title>Template-Gradle-Tomcat-Hibernate-Struts2</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="initial-scale=1, shrink-to-fit=no, width=device-width" name="viewport">

    <!-- CSS -->

    <!--Personal Css -->
    <link href="css/personal.css" rel="stylesheet"/>
    <!-- Add Material font (Roboto) and Material icon as needed -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i|Roboto+Mono:300,400,700|Roboto+Slab:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Add Material CSS, replace Bootstrap CSS -->
    <link href="css/material.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark-3">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
        </li>
    </ul>
</nav>

<div class="flex-row">
    <div class="alert">
                <span class="badge badge-info">
                    <h1>Template-Gradle-Tomcat-Hibernate-Struts2 <strong>Author Vincent Palazzo<strong> <a href="https://vincenzopalazzo.github.io">Visit my site</a></h1>
                </span>
    </div>
    <div class="alert">
                <span class="badge badge-dark">
                    <h2>This template using a bootstrap, the project is disponible <a href="https://daemonite.github.io/material/">here</a></h2>
                </span>
        </div>
    </div>

    <s:form action="register" cssClass="form-example">
        <p>This is an example for using personal bootstrap into struts2 taglib</p>
        <div class="form-group">
            <label for="exampleFormControlInput1">Email address</label>
            <s:textfield name="personBean.firstName" id="exampleFormControlInput1" cssClass="form-control" theme="simple" label="First name" />
        </div>
        <div class="form-group">
            <label for="password_id">Password</label>
            <s:password name="personBean.firstName" id="password_id" cssClass="form-control" theme="simple" label="First name" />
        </div>
        <s:submit cssClass="btn btn-dark" theme="simple"/>
    </s:form>

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

    <!-- Then Material JavaScript on top of Bootstrap JavaScript -->
    <script src="js/material.js"></script>
</body>
</html>