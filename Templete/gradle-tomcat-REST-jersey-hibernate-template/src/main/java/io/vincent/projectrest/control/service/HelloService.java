package io.vincent.projectrest.control.service;


import io.vincent.projectrest.model.HelloClass;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.util.GregorianCalendar;

/**
 * @author https://github.com/vincenzopalazzo
 */
@Path("/hello")
public class HelloService {

    private HelloClass helloClass = new HelloClass("Vincenzo", "Palazzo",
                            "https://github.com/vincenzopalazzo",
                                                new GregorianCalendar());

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getServiceHello(){

        return helloClass.toString();
    }
}
