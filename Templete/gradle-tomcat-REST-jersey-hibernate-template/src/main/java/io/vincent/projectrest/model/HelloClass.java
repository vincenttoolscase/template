package io.vincent.projectrest.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Calendar;

/**
 * @author https://github.com/vincenzopalazzo
 */
@XmlRootElement
public class HelloClass {

    private String name;
    private String surname;
    private String referenceGithub;
    private Calendar lastUpdate;

    public HelloClass(String name, String surname, String referenceGithub, Calendar lastUpdate) {
        this.name = name;
        this.surname = surname;
        this.referenceGithub = referenceGithub;
        this.lastUpdate = lastUpdate;
    }

    public HelloClass(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getReferenceGithub() {
        return referenceGithub;
    }

    public Calendar getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{\n");
        sb.append(name);
        sb.append(",\n").append(surname);
        sb.append(",\n").append(referenceGithub).append('\n');
        sb.append('}');
        return sb.toString();
    }
}
