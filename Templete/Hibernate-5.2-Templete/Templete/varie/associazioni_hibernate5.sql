
    create table Associazione (
       id int8 not null,
        annoFondazione int4 not null,
        codice varchar(255),
        nome varchar(255),
        primary key (id)
    );

    create table hibernate_sequences (
       sequence_name varchar(255) not null,
        next_val int8,
        primary key (sequence_name)
    );

    create table Iscrizione (
       id int8 not null,
        dataIscrizione date,
        associazione_id int8,
        persona_id int8,
        primary key (id)
    );

    create table Persona (
       id int8 not null,
        codiceFiscale varchar(255),
        cognome varchar(255),
        eta int4 not null,
        nome varchar(255),
        regione varchar(255),
        sesso varchar(255),
        primary key (id)
    );

    alter table Associazione 
       add constraint UK_d1ooycfg0dk7xgfbjr10v81th unique (codice);

    alter table Persona 
       add constraint UK_fsl0e0s3p6old4pv356my0q9b unique (codiceFiscale);

    alter table Iscrizione 
       add constraint FK5mmhw7jp20nqd5ulvsrrt9m4b 
       foreign key (associazione_id) 
       references Associazione;

    alter table Iscrizione 
       add constraint FK3xfgdyyc6af47pupi7ktp8s6o 
       foreign key (persona_id) 
       references Persona;
