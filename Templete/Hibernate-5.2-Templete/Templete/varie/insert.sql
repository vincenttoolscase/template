BEGIN TRANSACTION;
insert into Persona(id, codicefiscale, cognome, nome, eta, sesso, regione) values (-1, 'RSSMRX', 'Rossi', 'Mario', 30, 'M', 'Basilicata');
insert into Persona(id, codicefiscale, cognome, nome, eta, sesso, regione) values (-2, 'RSSMRC', 'Rossi', 'Marco', 20, 'M', 'Basilicata');
insert into Persona(id, codicefiscale, cognome, nome, eta, sesso, regione) values (-3, 'RSSMRA', 'Rossi', 'Maria', 33, 'F', 'Puglia');
insert into Persona(id, codicefiscale, cognome, nome, eta, sesso, regione) values (-4, 'VRDMRX', 'Verdi', 'Mario', 24, 'M', 'Basilicata');

insert into Associazione(id, codice, nome, annoFondazione) values (-1, 'AS01', 'A.V.I.S.', 1927);
insert into Associazione(id, codice, nome, annoFondazione) values (-2, 'AS02', 'A.N.T', 1950);
insert into Associazione(id, codice, nome, annoFondazione) values (-3, 'AS03', 'ARCI', 1983);
insert into Associazione(id, codice, nome, annoFondazione) values (-4, 'AS04', 'Pro Loco', 1990);

-- insert into Iscrizione(id, persona_id, associazione_id, dataIscrizione) values (1, 1, 1, '2017-3-6');
-- insert into Iscrizione(id, persona_id, associazione_id, dataIscrizione) values (2, 1, 2, '2017-3-7');
-- insert into Iscrizione(id, persona_id, associazione_id, dataIscrizione) values (3, 1, 3, '2017-3-8');
-- insert into Iscrizione(id, persona_id, associazione_id, dataIscrizione) values (4, 2, 1, '2017-3-8');

-- Importante --
-- I prossimi inserimenti servono a dire ad Hibernate di non iniziare da 1 a generare gli id (in pratica diciamo che il primo blocco di id è stato già utilizzato)
-- Altrimenti gli eventuali inserimenti che faremo dall'applicazione andranno in conflitto con quelli creati qui
-- INSERT INTO hibernate_sequences VALUES('Persona', 51);
-- INSERT INTO hibernate_sequences VALUES('Associazione', 51);
-- INSERT INTO hibernate_sequences VALUES('Iscrizione', 51);
COMMIT;