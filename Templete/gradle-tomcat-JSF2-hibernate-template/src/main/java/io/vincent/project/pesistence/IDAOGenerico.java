package io.vincent.project.pesistence;

import io.vincentpalazzo.template.pesistence.exceptions.DAOException;

import java.util.List;

public interface IDAOGenerico<T> {
    
     T findById(Long id, boolean lock) throws DAOException;
    
     List<T> findAll() throws DAOException;
    
     List<T> findAll(int offset, int limite) throws DAOException;

     T makePersistent(T entity) throws DAOException;
    
     void makeTransient(T entity) throws DAOException;
    
     void lock(T entity) throws DAOException;
}
