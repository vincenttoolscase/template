package io.vincent.project.model;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class Model {

    private User user;

    //getter and setter
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
