package it.unibas.progetto.controllo.filtri;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class FiltroCharsetEncoding implements Filter {

    private String encoding;

    public void destroy() {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
            FilterChain filterChain) throws IOException, ServletException {

        //System.out.println("*** Start CharsetEncodingFilter");
        servletRequest.setCharacterEncoding(encoding);
//        servletRequest.setCharacterEncoding("ISO-8859-1");
        servletResponse.setContentType("text/html; charset=UTF-8");
        //System.out.println("*** End CharsetEncodingFilter");

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void init(FilterConfig config) throws ServletException {

        encoding = config.getInitParameter("requestEncoding");

        if (encoding == null) {
            encoding = "UTF-8";
        }

    }
}