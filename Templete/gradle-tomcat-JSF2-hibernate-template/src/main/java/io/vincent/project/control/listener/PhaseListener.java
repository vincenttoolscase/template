package io.vincent.project.control.listener;

import java.util.Iterator;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PhaseListener implements javax.faces.event.PhaseListener {
    
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    public PhaseId getPhaseId() {
        return javax.faces.event.PhaseId.ANY_PHASE;
    }
    
    public void beforePhase(PhaseEvent phaseEvent) {
        if (phaseEvent.getPhaseId().equals(PhaseId.RESTORE_VIEW)) {
            LOGGER.debug("\n");
        }
        LOGGER.debug("Start Fase: " + phaseEvent.getPhaseId());
        ispeziona(phaseEvent);
    }
    
    public void afterPhase(PhaseEvent phaseEvent) {
        LOGGER.debug("End fase: " + phaseEvent.getPhaseId());
        ispeziona(phaseEvent);
    }
    
    private void ispeziona(PhaseEvent phaseEvent) {
        if (phaseEvent.getPhaseId().equals(PhaseId.RESTORE_VIEW) ||
                phaseEvent.getPhaseId().equals(PhaseId.RENDER_RESPONSE)) {
            FacesContext facesContext = phaseEvent.getFacesContext();
            UIViewRoot root = facesContext.getViewRoot();
            if (root == null) {
                LOGGER.debug("-- UIViewRoot null");
            } else {
                LOGGER.debug("-- UIViewRoot: " + root.getViewId());
                //printChild(root);
                UIForm form = (UIForm) findChild(root, "form");
                if (form != null) {
                }
            }
        }
    }
    
    private UIComponent findChild(UIComponent component, String id) {
        List listaFigli = component.getChildren();
        for (Iterator it = listaFigli.iterator(); it.hasNext(); ) {
            UIComponent figlio = (UIComponent)it.next();
            if (figlio.getId().equals(id)) {
                return figlio;
            }
        }
        return null;
    }
    
    private void printChild(UIComponent componente) {
        List listaFigli = componente.getChildren();
        for (Iterator it = listaFigli.iterator(); it.hasNext(); ) {
            UIComponent figlio = (UIComponent)it.next();
            LOGGER.debug("Componente figlio di viewRoot: " + figlio);
        }
    }
    
}
