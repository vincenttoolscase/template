package io.vincent.project.pesistence.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import io.vincent.project.pesistence.IDAOGenerico;
import io.vincentpalazzo.template.pesistence.exceptions.DAOException;
import org.hibernate.HibernateException;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DAOGenericoHibernate<T> implements IDAOGenerico<T> {

    private final static Logger LOGGER = LoggerFactory.getLogger(DAOGenericoHibernate.class);
    private Class<T> persistentClass;

    @SuppressWarnings("unchecked")
    public DAOGenericoHibernate(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    protected Class<T> getPersistentClass() {
        return persistentClass;
    }

    protected static Session getSession() throws DAOException {
        try {
            return DAOUtilHibernate.getCurrentSession();
        } catch (HibernateException ex) {
            LOGGER.error(ex.toString());
            throw new DAOException(ex);
        }
    }

    @SuppressWarnings("unchecked")
    public T makePersistent(T entity) throws DAOException {
        try {
            getSession().saveOrUpdate(entity);
        } catch (HibernateException ex) {
            LOGGER.error(ex.toString());
            throw new DAOException(ex);
        }
        return entity;
    }

    public void makeTransient(T entity) throws DAOException {
        try {
            getSession().delete(entity);
        } catch (HibernateException ex) {
            LOGGER.error(ex.toString());
            throw new DAOException(ex);
        }
    }

    @SuppressWarnings("unchecked")
    public void lock(T entity) throws DAOException {
        try {
            getSession().buildLockRequest(LockOptions.UPGRADE).lock(entity);
        } catch (HibernateException ex) {
            LOGGER.error(ex.toString());
            throw new DAOException(ex);
        }
    }

    @SuppressWarnings("unchecked")
    public T findById(String id) throws DAOException {
        Long idValue = Long.parseLong(id);
        List<T> lista = findByEqual("id", idValue);
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public T findById(Long id, boolean lock) throws DAOException {
        T entity;
        try {
            if (lock) {
                entity = (T) getSession().get(getPersistentClass(), id, LockOptions.UPGRADE);
            } else {
                entity = (T) getSession().get(getPersistentClass(), id);
            }
//            if (lock) {
//                entity = (T) getSession().load(getPersistentClass(), id, LockOptions.UPGRADE);
//            } else {
//                entity = (T) getSession().load(getPersistentClass(), id);
//            }
        } catch (HibernateException ex) {
            LOGGER.error(ex.toString());
            throw new DAOException(ex);
        }
        return entity;
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() throws DAOException {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(persistentClass);
        Root<T> root = criteria.from(persistentClass);
        criteria.select(root);
        return getSession().createQuery(criteria).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll(int offset, int limite) throws DAOException {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(persistentClass);
        Root<T> root = criteria.from(persistentClass);
        criteria.select(root);
        Query<T> query = getSession().createQuery(criteria);
        query.setFirstResult(offset);
        query.setMaxResults(limite);
        return query.getResultList();
    }

    public List<T> findByEqual(String attributeName, Object attributeValue) throws DAOException {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(persistentClass);
        Root<T> root = criteria.from(persistentClass);
        criteria.select(root);
        criteria.where(builder.equal(root.get(attributeName), attributeValue));
        return getSession().createQuery(criteria).getResultList();
    }

    /**
     * 
     * @param attributeName
     * @param attributeValue
     * @param isAsc = true se vogliamo un ordinamento crescente in base alla colonna attributeName
     * isAsc = false se vogliamo un ordinamento decrescente in base alla colonna attributeName
     * @return Lista ordinata in base alla scelta del parametro isAsc.
     * @throws DAOException 
     */
    public List<T> findByEqualOrderBy(String attributeName, Object attributeValue, boolean isAsc) throws DAOException {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(persistentClass);
        Root<T> root = criteria.from(persistentClass);
        criteria.select(root);
    if(isAsc){
        criteria.orderBy(builder.asc(root.get(attributeName)));
    }else{
        criteria.orderBy(builder.desc(root.get(attributeName)));
    }
        criteria.where(builder.equal(root.get(attributeName), attributeValue));
        return getSession().createQuery(criteria).getResultList();
    }

    public List<T> findByEqual(Map<String, Object> andSelections) throws DAOException {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(persistentClass);
        Root<T> root = criteria.from(persistentClass);
        criteria.select(root);
        List<Predicate> predicates = new ArrayList<Predicate>();
        for (String attributeName : andSelections.keySet()) {
            Object attributeValue = andSelections.get(attributeName);
            predicates.add(builder.equal(root.get(attributeName), attributeValue));
        }
        Predicate andPredicate = builder.and(predicates.toArray(new Predicate[]{}));
        criteria.where(andPredicate);
        return getSession().createQuery(criteria).getResultList();
    }

    public List<T> findByEqualIgnoreCase(String attributeName, String attributeValue) throws DAOException {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(persistentClass);
        Root<T> root = criteria.from(persistentClass);
        criteria.select(root);
        Expression<String> path = root.get(attributeName);
        Expression<String> upper = builder.upper(path);
        Predicate ctfPredicate = builder.equal(upper, attributeValue.toUpperCase());
        criteria.where(builder.and(ctfPredicate));
        return getSession().createQuery(criteria).getResultList();
    }

    @SuppressWarnings("unchecked")
    public T saveOrMerge(T obj, Long id) throws DAOException {
        try {
            T persistentObject = (T) getSession().get(persistentClass, id);
            if (persistentObject != null) {
                if (LOGGER.isDebugEnabled()) LOGGER.debug("Get ha trovato l'oggetto con id " + id);
                return persistentObject;
            } else {
                makePersistent(obj);
                return obj;
            }
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            throw new DAOException(ex);
        }
    }

    @SuppressWarnings("unchecked")
    public T merge(T obj) throws DAOException {
        try {
            T persistentObject = (T) getSession().merge(obj);
            return persistentObject;
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            throw new DAOException(ex);
        }
    }


}
