package io.vincent.project.control.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import io.vincent.project.pesistence.hibernate.DAOUtilHibernate;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FiltroHibernate implements Filter {

    private static Logger LOGGER = LoggerFactory.getLogger(FiltroHibernate.class);

    private SessionFactory sessionFactory;

    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.debug("Initializing io.vincent.project.control.filter...");
        LOGGER.debug("Obtaining SessionFactory from static HibernateUtil singleton");
        sessionFactory = DAOUtilHibernate.getSessionFactory();
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        try {
            LOGGER.debug("Starting a database transaction");
            sessionFactory.getCurrentSession().beginTransaction();

            chain.doFilter(request, response);

            LOGGER.debug("Committing the database transaction");
            sessionFactory.getCurrentSession().getTransaction().commit();
        } catch (StaleObjectStateException staleEx) {
            LOGGER.error("This interceptor does not implement optimistic concurrency control!");
            LOGGER.error("Your application will not work until you add compensation actions!");
            throw staleEx;
        } catch (Throwable ex) {
            ex.printStackTrace();
            try {
                if (sessionFactory.getCurrentSession().getTransaction().isActive()) {
                    LOGGER.debug("Trying to rollback database transaction after exception");
                    sessionFactory.getCurrentSession().getTransaction().rollback();
                }
            } catch (Throwable rbEx) {
                LOGGER.error("Could not rollback transaction after exception!", rbEx);
            }
            throw new ServletException(ex);
        }
    }

    public void destroy() {}

}

